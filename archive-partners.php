<?php
    /**
    * Template Name: Colaboradores Archive
    */
?>
	<?php get_header(); ?>
	<?php get_template_part( 'elements/ads', '720-home-top') ?>
	<?php get_template_part( 'elements/ads', 'header-home-mobile') ?>
	<div class="container" id="mainContent">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="">
				<h1 class="h1">
					<?php the_title(); ?>
				</h1>
				<?php the_content(); ?>
				<div id="contributorsBox">
					<?php get_template_part( 'elements/content','unique-contributor'); ?>
				</div>
			</div>
		</div>
	</div>

	<?php get_template_part( 'elements/ads', '720-home-middle') ?>
	<?php get_template_part( 'elements/ads', 'header-home-mobile') ?>
	<?php get_template_part( 'elements/section', 'instagram') ?>
	<?php get_template_part( 'elements/section', 'partnerSites') ?>
	<?php get_template_part( 'elements/section','socialMedia' ); ?>

	<?php get_footer(); ?>
