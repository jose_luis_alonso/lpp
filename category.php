<?php
/*
Theme Name: LPP2017
Author: Mau Ferrusca / Dorian Martínez
Author URI: http://wordpress.org/
Description:  Skin responsivo y theme para LPP 2017.
Version: 1.0
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: white, responsive, bootstrap, ACF
*/

 get_header(); ?>

	<?php get_template_part( 'elements/ads', 'skin-banner') ?>
	
	<?php
function catName(){
	$category = get_queried_object();
	echo '<span>' . $category->cat_name. '</span>';
}
?>
		<?php get_template_part( 'elements/ads', '720-home-top') ?>
		<?php get_template_part( 'elements/ads', 'header-home-mobile') ?>
		<div class="container" id="mainContent">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="catego-main-name">
						<?php catName(); ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" id="leftContent">
					<?php get_template_part('elements/content','mainArticleCat') ?>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 hidden-sm hidden-xs" id="sideBarRight">

					<div class="minuto-a-minuto-component">
						<div class="row">
							<?php get_template_part( 'elements/section', 'minuteByMinute'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container" id="categoryBoxes">
			<div class="row">
				<?php get_template_part( 'elements/content', 'categoryCardCat'); ?>
				<?php get_template_part( 'elements/content', 'categoryCard2ndRowCat'); ?>
			</div>
		</div>
		<?php get_template_part( 'elements/ads', '720-home-bottom') ?>
		<div class="container" id="masReciente">
			<div class="row">
				<div class="col-lg-12">
					<h2 class="h2">Las noticias más recientes</h2>
				</div>
				<?php get_template_part( 'elements/content' , 'newsRecentMain') ?>
				<?php get_template_part( 'elements/ads', 'square-recent-sidebar') ?>
				<div class="col-sm-8" id="masRecienteList">
					<div class="row">
						<?php get_template_part( 'elements/content' , 'newsRecentListedCat') ?>
					</div>
				</div>
				<div class="col-sm-4" id="masViral">
					<h2 class="h2">Lo más compartido</h2>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="masViralList">
						<ul>
							<?php get_template_part( 'elements/content' , 'viralListedCat') ?> </ul>
					</div>
				</div>
			</div>
		</div>
		<?php get_template_part( 'elements/ads', '720-home-bottom') ?>
		<?php get_template_part( 'elements/section', 'facebook') ?>
		<?php get_template_part( 'elements/section', 'instagram') ?>
		<?php get_template_part( 'elements/section', 'socialMedia') ?>
		<?php get_footer(); ?>
