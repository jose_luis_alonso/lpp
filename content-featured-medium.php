<?php
//CAT
$cat_name = get_first_category($post->ID);

//Cat icon
$cat_icon = get_cat_icon($post->ID);

//ICON
$icon = 'fa-chevron-right';
if( has_post_format( 'video' ) ) {
	$icon = 'fa-youtube-play';
} elseif( has_post_format( 'gallery' ) ) {
	$icon = 'fa-image';
}
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('post-item post-item-featured-medium'); ?>>
	<a href="<?php the_permalink(); ?>" class="post-item-thumb" style="background-image:url(<?php echo get_post_image($post->ID, 'thumb-big'); ?>);">
		<span class="post-item-caption" <?php if($cat_icon){ echo 'style="padding-right:80px;"'; } ?>>
			<?php if($cat_name) { ?><span class="post-item-cat" style="<?php echo get_cat_color($post->ID); ?>"><?php echo $cat_name; ?></span><em class="post-item-type"><i class="fa fa-fw <?php echo $icon; ?>"></i></em><?php } ?>
			<strong class="post-item-title"><span><?php the_title(); ?></span></strong>
			<?php echo $cat_icon; ?>
		</span>
	</a>
</article>
