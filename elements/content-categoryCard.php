<?php
	$args = array(
	'post_type' => 'post',
	'posts_per_page' => 4,
	'featured' => 'featured',
	'post_status' => 'publish',
	'offset' => 1,
	);


	$categoCard = new WP_Query($args);

echo '<div class="row">';

	if( $categoCard->have_posts() ):
	while ( $categoCard-> have_posts()) : $categoCard->the_post();

echo '
			<div class="col-sm-3">
			<div class="categoryCard">
				<a href="' . get_permalink() . '">
				<div class="img" style="background-image: url(' . get_the_post_thumbnail_url() . ');">
				</div>
				</a>
				<div class="texts">
					<div class="tag-id">
						<div class="catego-tag">';
								$category = get_the_category();
                echo '<a href="'.get_category_link($category[0]->cat_ID).'">' . $category[0]->cat_name . '</a>';
			echo '</div>
				</div>
				<div class="excerpt">
					<a href="' . get_permalink() . '">
						<p>'; echo wp_html_excerpt( get_the_title(), 50, '...' ); echo '</p>
					</a>
					</div>
				</div>
			</div>
			</div>
			';
	endwhile;wp_reset_query();
	endif;
echo'
		</div>'	;
?>
