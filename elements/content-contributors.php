<?php
// WP_User_Query arguments

$users_per_page =4;
$args = array (
    'role'       => 'author',
    'order'      => 'ASC',
    'orderby'    => 'display_name',
		'number' => $users_per_page
	);	

// Create the WP_User_Query object
$wp_user_query = new WP_User_Query( $args );

// Get the results
$authors = $wp_user_query->get_results();
// Check for results
if ( ! empty( $authors ) ) {
    // loop through each author
	
	echo '<div class="container" id="colaboradores">
	<div class="row">
		<h2 class="h2 text-center">Colaboradores</h2> ';
    foreach ( $authors as $author ) {
			$user_info = get_userdata( $author->ID );
//			$authorThumb = get_avatar_url();
			
        // get all the user's data
			echo '
			<div class="col-xs- col-sm- col-md-3 col-lg-3">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="avatarImg img-circle center-block" style=" background-image:url('. get_field('user_avatar', $user_info) .') "></div>
					
					<div class="colabTab">
					<div class="colabName text-center">' . $user_info->first_name . ' ' . $user_info->last_name . '</div>
				<div class="colabSubtitle text-center">' . get_field('descripcion_de_usuario', $user_info) . '</div>
				</div>
			</div>
		</div>
			';
    }
	echo '
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4" id="colabButtonBox"> <a id="colabButton" class="btn btn-default" href=" '; echo bloginfo('wpurl') .'/colaboradores/">Ver todos los colaboradores</a> </div>
	</div>
</div>';
}
else {
    echo 'No se encontraron autores.';
}			

?>
