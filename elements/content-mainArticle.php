<?php
	$args = array(
	'post_type' => 'post',
	'posts_per_page' => 1,
	'featured' => 'featured',
		'post_status' => 'publish',
//		'cat' => 1 //noticias
	);


	$lastPost = new WP_Query($args);

	if( $lastPost->have_posts() ):
	echo '<div id="mainArticleBox">';
	while( $lastPost->have_posts() ): $lastPost->the_post();
	//	get_template_part('content', 'featured-big');
	if (has_post_thumbnail()) :
	$category = get_the_category( $id );
	echo '
	
	<div class="hero-img" style="background-image: url('
		. get_the_post_thumbnail_url() . 
		');">
			<div class="tag-id hidden-xs hidden-sm">';
					$category = get_the_category();
					echo '<a href="'.get_category_link($category[0]->cat_ID).'">' . $category[0]->cat_name . '</a>
			</div>
		</div>
	';
		echo '
			<div class="mainArticle-texts">
				<h1 class="h1"><a href="' . get_permalink() . '">' . get_the_title(). '</a></h1>
				<div class="excerpt">
						<a href="' . get_permalink() . '"> <span>' . excerpt(30) . '</span> </a>
				</div>
			</div>
		</div>';

	endif;
	endwhile;
	endif;
wp_reset_postdata();
	?>