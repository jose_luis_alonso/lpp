<?php
	$args = array(
	'post_type' => 'partner',
	'posts_per_page' => 4,
		'orderby' => 'DESC',
		'post_status' => 'publish',
//	'featured' => 'featured',
//	'offset' => 1,
	);


	$partner = new WP_Query($args);


	if( $partner->have_posts() ):
	while ( $partner-> have_posts()) : $partner->the_post();
	$partnerUrl = get_field( 'url_de_partner' );
	$partnerTitle = get_the_title();
	$partnerThumb = get_the_post_thumbnail_url();
echo '		
<div class="col-sm-3 categoryCard">
					<div class="img" style="background-image: url('. $partnerThumb .') " > 
					</div>
					<div class="texts">
						<div class="tag-id"> <a href=" ' . $partnerUrl . ' " target="_blank" >' . $partnerTitle . '</a> </div>
						<div class="excerpt">
							<p>' . get_the_content() . '</p>
						</div>
					</div>
				</div>
			';
	endwhile;wp_reset_query();
	endif;
?>