<?php
$users_per_page =6;
$args = array (
    'role'       => 'author',
    'order'      => 'ASC',
    'orderby'    => 'display_name',
	'post_status' => 'publish',
		'number' => $users_per_page
	);

// Create the WP_User_Query object
$wp_user_query = new WP_User_Query( $args );

// Get the results
$authors = $wp_user_query->get_results();
// Check for results
if ( ! empty( $authors ) ) {
    // loop through each author
    foreach ( $authors as $author ) {
			$user_info = get_userdata( $author->ID );
			$authorThumb = get_avatar_url();
echo '
<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 contributor">
	<div class="imgBox img-circle center-block" style="background-image: url('. get_field('user_avatar', $user_info) .') " >
		
	</div>
	<div class="contributor">
	<div class="name">
		'. $user_info->first_name . ' ' . $user_info->last_name . '
	</div>
		<div class="social-icons">';
			$fbUrl = get_field( 'fb_url', $user_info);
			$twUrl = get_field( 'tw_url', $user_info);
			$igUrl = get_field( 'ig_url', $user_info);
	
			if ( $fbUrl ):
				echo '<a class="btn btn-default fb" href="' . $fbUrl . '" role="button" target="_blank"><img src="' .  get_template_directory_uri() . '/assets/facebook.png" class="img-responsive" alt="Image"></a>';
				endif;
			
			if ( $twUrl ):
				echo '<a class="btn btn-default tw" href="' . $twUrl . '" role="button" target="_blank"><img src="' .  get_template_directory_uri() . '/assets/twitter.png" class="img-responsive" alt="Image"></a>';
				endif;
			
			if ( $igUrl ):
				echo '<a class="btn btn-default ig" href="' . $igUrl . '" role="button" target="_blank"><img src="' .  get_template_directory_uri() . '/assets/ig-glyph-30-px@2x.png" class="img-responsive" alt="Image"></a>';
				endif;
			
			echo'				
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="contribPostList">
							<ul>';
			$postArgs = array (
				'author__in'=> $user_info->ID, //Authors's id's you like to include
				'post_type' => 'post',
				'posts_per_page' => 5,
			);
			
			$postsquery = new WP_Query ( $postArgs); 
			
			while ( $postsquery->have_posts() ) : $postsquery->the_post();
			
			$category = get_the_category( $id );
echo '
		<li class="viral-note contributor">
			<div class="row no-gutter">
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 square-mini img-responsive" style="background-image: url(' . get_the_post_thumbnail_url() . ')" > </div>
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<span class="catego-tag">'; 
					$category = get_the_category();
					echo '<a href="'.get_category_link($category[0]->cat_ID).'">' . $category[0]->cat_name . '</a>
			</span>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<span class="excerpt"> <a href="' . get_permalink() . '" target="_blank"> ' . get_the_title() . '</a>  </span>
					</div>
				</div>
			</div>
		</li>';
			endwhile;wp_reset_query();
		echo '
			</ul>
		</div>
	</div>
</div>
';
   }
	}
	else {
		echo 'No se encontraron autores.';
	}	
?>
