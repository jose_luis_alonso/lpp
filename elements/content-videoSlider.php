<?php
	$args = array(
	'post_type' => 'ytvideo',
	'posts_per_page' => 4,
		'post_status' => 'publish',
//	'featured' => 'featured',
//	'cat' => 7, //entretenimiento
//	'offset' => 1,
	);


	$videoSlide = new WP_Query($args);

echo '<div class="container-fluid section  hidden-xs hidden-sm" id="videoCaroussel">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hidden-xs hidden-sm">
			<div id="videoTitle">
					<p class="title text-center"><span class="txt-highlight">Los videos más interesantes,</span> en nuestro canal de YouTube</p>
					<div class="g-ytsubscribe" data-channelid="UCNSoFp-qRiRUIcPbD898tuA" data-layout="default" data-count="default"></div>
				</div>
				<script src="https://apis.google.com/js/platform.js"></script>
			<section class="slider">
				<div class="flexslider">
					<ul class="slides text-center">';


	if( $videoSlide->have_posts() ):
	while ( $videoSlide-> have_posts()) : $videoSlide->the_post();

			$videoUrl = get_field ( 'url_de_video' );
			$videoTitle = get_field ( 'titulo_de_video' );
			$videoDescript = get_field ( 'descripcion_de_video' );

echo'
			<li>
				<!-- Video Card for slider -->
				<div class="videoCard">
					<div class="heroThumb">
						<div class="embed-responsive embed-responsive-16by9">
							<iframe width="585" height="366" src="https://www.youtube.com/embed/' . $videoUrl . '" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
					<div class="title text-center">
						<p>' . get_the_title() . '</p>
					</div>
					<div class="descript text-center">
						<p>' . $videoDescript . '</p>
					</div>
				</div>
			</li>
			';
	endwhile;wp_reset_query();
	endif;

			echo'		</ul>
				</div>
			</section>
		</div>
	</div>
</div>';

?>
