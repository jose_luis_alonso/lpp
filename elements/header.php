<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>
		<?php wp_title( '|', true, 'right') ?>
		<?php bloginfo( 'name'); ?>
	</title>
	<?php wp_head(); ?>
	<link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_directory')?>/assets/favicon/favicon-16x16.png">
</head>

<body <?php body_class(); ?> >
	<div class="container-fluid" id="navBar">
		<!--	<div class="container-fluid" id="navBar">-->
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<nav class="navbar navbar-default" role="navigation">
					<!--				<nav class="navbar navbar-default small" role="navigation">-->
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
						<a class="navbar-brand" href="<?php bloginfo('url')?>">
<!--						<img src="<?php bloginfo('template_directory')?>/assets/lpp-logo-one-line.png" alt="" class="img-responsive">-->
						</a>

						<!--
						<div id="searchIcon" class="socialmedia-square ">
							<a href="#"><img src="<?php bloginfo('template_directory')?>/assets/header/Search_18px.png" alt="" class=""></a>
						</div>
-->
					</div>
					<div id="mySearchBox" class="hidden-lg hidden-md hidden-sm">
						<?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse navbar-ex1-collapse">
						<?php
							$args = array (
								'menu' => 'header_menu',
								'menu_class' => 'nav navbar-nav',
							'container' => 'true'
							);
							 wp_nav_menu( $args );
							?>
					</div>
					<div class="socialmedia-group flex-container flex-row hidden-xs">
						<div class="socialmedia-square hidden-xs">
							<a href="<?php the_field('facebook_global_url'); ?>" target="_blank">
						<img src="<?php bloginfo('template_directory')?>/assets/header/Facebook_30px.png" alt="">
					</a>
						</div>
						<div class="socialmedia-square hidden-xs">
							<a href="<?php the_field('twitter_global_url'); ?>" target="_blank">
						<img src="<?php bloginfo('template_directory')?>/assets/header/Twitter_30px.png" alt="">
							</a>
						</div>
						<div class="socialmedia-square hidden-xs ">
							<a href="<?php the_field('instagram_global_url'); ?>" target="_blank">
						<img src="<?php bloginfo('template_directory')?>/assets/header/IG_30px.png" alt="">
				</a>
						</div>
						<div class="socialmedia-square hidden-xs" id="searchToggle">
							<a href="#"><img src="<?php bloginfo('template_directory')?>/assets/header/Search_18px.png" alt="" class=""></a>
						</div>


					</div>
					<!-- /.navbar-collapse -->
				</nav>
				<div id="searchBox" class="">
					<?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
				</div>
			</div>
		</div>
	</div>