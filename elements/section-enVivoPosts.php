<?php

$category = get_the_category();
$catTerm = $category[0]->cat_name;
$today = getdate();

//$args = array(
//	'meta_query' => array(
//		array(
//			'key' => 'en_vivo',
//			'value' => '1',
//			'compare' => '=='
//		)
//	),
//	'post_type' => 'post',
//	'posts_per_page' => 6,
//	'featured' => 'featured',
//	'post_status' => 'publish',
//	'offset' => 1,
//	'category_name' => $catTerm,
//		'date_query' => array(
//			// POSTS FOR TODAY
//			//				array (
//			//					'year' => $today['year'] ,
//			//					'month=' => $today['mon'] ,
//			//					'day=' => $today['mday'] 
//			//				)
//			// POSTS FOR THIS WEEK
//				array(
//					'year' => date( '2016' ),
//					'week' => date( 'W' ),
//				),			
//			)
//		);
$args = array(
	'order' => 'DESC',
	'post_status' => 'publish',
	'posts_per_page' => 10,
	'meta_query' => array(
		array(
			'key' => 'en_vivo',
			'value' => '1',
			'compare' => '=='
		)
	));
?>
	<div id="enVivo">
		<?php
	$categoCard = new WP_Query($args);

	if( $categoCard->have_posts() ):
		?>
		<?php
		while ( $categoCard-> have_posts()) : $categoCard->the_post();

echo '<div class="col-sm-12 minuto-a-minuto-item">
	<div class="col-sm-2">
		<div class="count-back">
		Hace ' . human_time_diff( get_the_time('U'), current_time('timestamp') ) .'
		</div>
		<span class="timer">'. get_the_date('H:i') . '</span>
	<div class="social-micro">
		<div class="social-micro-box fb">
			<div class="fb-share-button" data-href="http://dev-laprimeraplana.dosdev.com/" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fdev-laprimeraplana.dosdev.com%2F&amp;src=sdkpreparse">Compartir</a>
			</div>
</div>
<div class="social-micro-box tw">
	';

echo do_shortcode('[twitter_share]');
echo'
</div>

</div>
	</div>
	<div class="col-sm-10">
		<h4 class="h4">
			<a href="' . get_permalink() . '">
							<p class="titleText">' . get_the_title() . '</p>
						</a>
		</h4>
		<div class="img" style="background-image: url(' . get_the_post_thumbnail_url() . ');">
				</div>
		</div>
</div>';

	endwhile;wp_reset_query();
	endif;
echo'
</div>'
?>
