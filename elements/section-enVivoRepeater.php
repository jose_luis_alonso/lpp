<ul>
	<!-- Start Repeater -->
	<?php
//$user_id = get_current_user_id();
if( have_rows('articulos_repeater')): // check for repeater fields ?>

		<?php while ( have_rows('articulos_repeater')) : the_row(); // loop through the repeater fields ?>

		<?php // set up post object
        $post_object = get_sub_field('posts_relacionados');
        if( $post_object ) :
        $post = $post_object;
        setup_postdata($post);
        ?>
		<?php
	echo '<div class="col-sm-12 minuto-a-minuto-item">
	<div class="col-sm-2">
		<div class="count-back">
		Hace ' . human_time_diff( get_the_time('U'), current_time('timestamp') ) .'
		</div>
		<span class="timer">'. get_the_date('H:i') . '</span>
	<div class="social-micro">
		<div class="social-micro-box fb">
			<div class="fb-share-button" data-href="http://dev-laprimeraplana.dosdev.com/" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fdev-laprimeraplana.dosdev.com%2F&amp;src=sdkpreparse">Compartir</a>
			</div>
</div>
<div class="social-micro-box tw">';

echo do_shortcode('[twitter_share]');
echo'
</div>

</div>
	</div>
	<div class="col-sm-10">
		<h4 class="h4">
			<a href="' . get_permalink() . '">
							<p class="titleText">' . get_the_title() . '</p>
						</a>
		</h4>
		<div class="img" style="background-image: url(' . get_the_post_thumbnail_url() . ');">
				</div>
		</div>
</div>';
	
?>
			<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

			<?php endif; ?>

			<?php endwhile; ?>

			<!-- End Repeater -->
			<?php endif; ?>
</ul>