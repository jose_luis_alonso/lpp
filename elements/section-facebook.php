<?php
$fbURL = get_field('facebook_global_url',get_option('page_on_front'));
?>
	<div class="container-fluid hidden-xs hidden-sm" id="fbLike">
		<div class="row">
			<div class="col-sm-2 col-lg-2">
				<div class="flexBox">
					<div class="flexBox-item">
						<a href="<?php echo $fbURL ?>">
							<img src="<?php bloginfo('template_directory')?>/assets/fb-thumb.png" alt="" class="img-responsive">
						</a>
					</div>
				</div>
			</div>
			<div class="col-sm-10 col-lg-10">
				<div class="flexBox">
					<div class="flexBox-item">
						<div id="fbTitle" class="text-center"> Las últimas noticias directamente en tu muro,
							<br>
							<a href="<?php echo $fbURL ?>">
								<span class="fbBold">a un like de distancia</span>
							</a>
						</div>
						<div id="fbLikeBtn" class="">
							<div class="fb-like" data-href="https://www.facebook.com/lppmx/" data-width="" data-layout="standard" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>