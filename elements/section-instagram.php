<?php 
	$IGUrl = get_field('instagram_global_url',get_option('page_on_front'));
?>
<div class="container-fluid" id="instaBlock">
	<div class="container-fluid insta" id="instagram">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-lg-offset-1  hidden-xs hidden-sm" id="instaLogo">
					<a href="<?php echo $IGUrl ?>">
					<img src="<?php bloginfo('template_directory')?>/assets/ig-100-px.png" class="img-responsive center-block" alt="">
			</a>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					<div class="instaTitle hidden-sm hidden-xs"> Disfruta de nuestro contenido exclusivo
						<br> en instagram: <span class="boldTxt hidden-xs hidden-sm">
					<a href="<?php echo $IGUrl ?>" target="_blank"><strong>@primeraplanamx</strong></a>
					</span>
					</div>
					<div class="instaTitle hidden-md hidden-lg text-center"> Disfruta de nuestro contenido exclusivo <strong>en Instagram:</strong> <span class="boldTxt hidden-xs hidden-sm">
					<a href="<?php echo $IGUrl ?>" target="_blank">@primeraplanamx</a>
					</span>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-lg-offset-1  hidden-md hidden-lg mobile" id="instaLogoMobile"> <img src="<?php bloginfo('template_directory')?>/assets/ig-100-px.png" class="img-responsive center-block" alt="">
					</div>
					<div class="boldTxt hidden-md hidden-lg text-center">
						<a href=" <?php echo $IGUrl ?>" class="" target="_blank">@primeraplanamx</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid insta" id="instaCaroussel">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div id="instafeed" class=""></div>
			</div>
		</div>
	</div>
</div>
<?php 
?>