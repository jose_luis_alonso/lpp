<?php

$category = get_the_category();
$catTerm = $category[0]->cat_name;
$today = getdate();

	$args = array(
	'post_type' => 'post',
	'posts_per_page' => 6,
	'featured' => 'featured',
	'post_status' => 'publish',
	'offset' => 1,
	'category_name' => $catTerm,
	'date_query' => array(
// POSTS FOR TODAY
//				array (
//					'year' => $today['year'] ,
//					'month=' => $today['mon'] ,
//					'day=' => $today['mday'] 
//				)
// POSTS FOR THIS WEEK
array(
			'year' => date( '2016' ),
			'week' => date( 'W' ),
		),			
		)
	);
?>

	<?php
	$categoCard = new WP_Query($args);

	if( $categoCard->have_posts() ):
?>
		<div class="col-sm-12 minuto-a-minuto-title">
			<div class="col-sm-2">
				<div class="ico">
					<img src="<?php bloginfo('template_directory')?>/assets/img-reloj.svg" alt="">
				</div>
			</div>
			<div class="col-sm-10">
				<h3 class="h3">Minuto a minuto</h3>
			</div>
		</div>
		<?php
	while ( $categoCard-> have_posts()) : $categoCard->the_post();

echo '<div class="col-sm-12 minuto-a-minuto-item">
	<div class="col-sm-2">
		<span class="timer">'. get_the_date('H:i') . '</span>
	</div>
	<div class="col-sm-10">
		<h4 class="h4">
			<a href="' . get_permalink() . '">
							<p>' . get_the_title() . '</p>
						</a>
		</h4>
	</div>
</div>';

	endwhile;wp_reset_query();
	endif;

?>
