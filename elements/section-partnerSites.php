<?php ?>
<div class="container-fluid  hidden-xs hidden-sm" id="otrosSitios">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center"> <img src="<?php bloginfo('template_directory')?>/assets/logo-astrea-header.png" alt="" class="src img-responsive center-block">
				<p class="title">Visita nuestros otros sitios</p>
			</div>
			<div class="container" id="categoryBoxes">
				<div class="row">
					<?php get_template_part( 'elements/content', 'partners'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php  ?>