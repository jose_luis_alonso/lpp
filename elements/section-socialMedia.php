<div class="container section" id="socialMedia">
	<div class="row">
		<div class="col-xs- col-sm- col-md- col-lg-">
			<h2 class="h2 text-center">¿Olvidaste añadirnos?</h2>
			<p class="title text-center">Disfruta del contenido de<br>La Primera Plana
				<br><span class="boldTxt">en tu red social favorita.</span></p>
			<div id="socialBtnBox" class="flex-parent flex-row flex-between hidden-xs hidden-sm">
				<a class="btn btn-default social-media-button  hidden-xs hidden-sm" id="facebook" href="<?php the_field('facebook_global_url', get_option('page_on_front')); ?>" role="button">
				<img src="<?php bloginfo('template_directory')?>/assets/facebook-icon.png" alt="">Facebook
				</a>
				<a class="btn btn-default social-media-button inverse  hidden-xs hidden-sm" id="instagram" href="<?php the_field('instagram_global_url', get_option('page_on_front'));?>" role="button">
				<img src="<?php bloginfo('template_directory')?>/assets/ig-glyph-30-px.png" alt="">Instagram
				</a>
				<a class="btn btn-default social-media-button  hidden-xs hidden-sm" id="twitter" href="<?php the_field('twitter_global_url', get_option('page_on_front')); ?>" role="button">
				<img src="<?php bloginfo('template_directory')?>/assets/icon.png" alt="">Twitter
				</a>
			</div>
			<div class="col-lg-3 col-lg-offset-1 flex-parent flex-row flex-around hidden-md  hidden-lg" id="footerSocial">
				<div class="social-square">
					<a href="<?php get_field('facebook_global_url', get_option('page_on_front')); ?>"><img src="<?php bloginfo('template_directory')?>/assets/footer/Facebook_50px.png" alt=""></a>
				</div>
				<div class="social-square">
					<a href="<?php the_field('twitter_global_url', get_option('page_on_front')); ?>"><img src="<?php bloginfo('template_directory')?>/assets/footer/Twitter_50px.png" alt=""></a>
				</div>
				<div class="social-square">
					<a href="<?php the_field('instagram_global_url', get_option('page_on_front')); ?>"><img src="<?php bloginfo('template_directory')?>/assets/footer/IG_50px.png" alt=""></a>
				</div>
			</div>
		</div>
	</div>
</div>
