<?php
	$args = array(
	'post_type' => 'post',
	'posts_per_page' => 4,
	'featured' => 'featured',
		'post_status' => 'publish',
//	'cat' => 7, //entretenimiento
	'offset' => 1
	);
	$categoCard = new WP_Query($args);
	echo'<div class="container" id="categoryBoxes">
		<div class="row">

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" id="categoAlert">
				<div>
					<span class="alert-color">¡Espera!</span>
				</div>
				<div>
					<span>Hay más contenido interesante </span>
				</div>
			</div>';
if( $categoCard->have_posts() ):
	while ( $categoCard-> have_posts()) : $categoCard->the_post();
$category = get_the_category( $id );
echo'
<div class="col-sm-3 categoryCard">
				<a href="' . get_permalink() . '">
				<div class="img" style="background-image: url(' . get_the_post_thumbnail_url() . ');">
				</div>
				</a>
				<div class="texts">
					<div class="tag-id">
						<div class="catego-tag">';
							$category = get_the_category();
							echo '<a href="'.get_category_link($category[0]->cat_ID).'">' . $category[0]->cat_name . '</a>';
					
					echo '</div>
					</div>
					<div class="excerpt">
						<a href="' . get_permalink() . '">
							<p>' . get_the_title() . '</p>
						</a>
					</div>
				</div>
			</div>
';
	endwhile;wp_reset_query();
echo '
	</div>
</div>';

endif;
?>