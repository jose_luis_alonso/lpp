<?php
/**
*Theme Name: LPP2017
*Author: Mau Ferrusca / Dorian Martínez
*Author URI: http://wordpress.org/
*Description:  Skin responsivo y theme para LPP 2017.
*Version: 1.0
*License: GNU General Public License v2 or later
*License URI: http://www.gnu.org/licenses/gpl-2.0.html
*Tags: white, responsive, bootstrap, ACF
*Template Name: En vivo
*/
get_header(); ?>

<?php get_template_part( 'elements/ads', '720-home-top') ?>
<?php get_template_part( 'elements/ads', 'header-home-mobile') ?>
	<!-- en vivo ticker via ACF -->
	<div class="container" id="enVivo">
		<div class="row">
			<div class="col-xs- col-sm- col-md- col-lg-12">
		<?php
		$tickerID = get_field('id_numerico_para_ticker');
		if(function_exists('ditty_news_ticker')){ditty_news_ticker( $tickerID );} ?>
				<div class="col-lg-12">
					<h3 class="h3 text-center">
						<?php the_title(); ?>
					</h3>
				</div>
			</div>
		</div>
	</div>
	<!--en vivo ticker end-->

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<?php
				function catName(){
					$category = get_the_category();
					echo '<span>' . $category[0]->name . '</span>';
				}
		?>
		<div class="container" id="enVivo">
			<div class="row">

				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" id="leftContent">
					<div id="mainNewsContent">
						<?php get_template_part('elements/section','enVivoRepeater');  ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" id="sideBarRight">
				<?php get_template_part( 'elements/ads', 'square-home-sidebar-bottom') ?>
				</div>
			</div>
		</div>
		<?php endwhile; ?>
		<?php endif; ?>
		<?php get_template_part( 'elements/section', 'waitContent') ?>
		<div class="container banner-large hidden-xs">
			<?php the_ad(143132); ?>
		</div>
		<?php get_template_part( 'elements/section', 'instagram') ?>
		<?php get_footer(); ?>
