<footer class="container-fluid">
	<div class="container footer">
		<div class="row">
			<div class="col-lg-3 col-md-3" id="footerLogo">
				<a href="<?php bloginfo('url');  ?>">
			  <img src="<?php bloginfo('template_directory')?>/assets/logos/logo-primera-plana.svg" alt="">
			  </a>
			</div>
			<div class="col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1" id="footerNav">
				<?php
							$argsFooter = array (
								'menu' => 'header_menu',
								'menu_class' => 'footer-nav',
								'container' => 'false'
							);
							 wp_nav_menu( $argsFooter );
							?>
			</div>
			<div class="col-md-3 col-md-offset-1 col-lg-3 col-lg-offset-1 flex-parent flex-row flex-around  hidden-xs hidden-sm" id="footerSocial">
				<div class="social-square">
					<a href="<?php the_field('facebook_global_url', get_option('page_on_front')); ?>" target="_blank">
						<img src="<?php bloginfo('template_directory')?>/assets/footer/Facebook_50px.png" alt="">
					</a>
				</div>
				<div class="social-square">
					<a href="<?php the_field('twitter_global_url', get_option('page_on_front')); ?>" target="_blank">
						<img src="<?php bloginfo('template_directory')?>/assets/footer/Twitter_50px.png" alt="">
				</a>
				</div>
				<div class="social-square">
					<a href="<?php the_field('instagram_global_url', get_option('page_on_front')); ?>" target="_blank">
						<img src="<?php bloginfo('template_directory')?>/assets/footer/IG_50px.png" alt="">
				</a>
				</div>
			</div>

			<!--	FOOTER DESKTOP	-->

			<div id="footerInfo" class="col-lg-12 col-md-12 hidden-sm hidden-xs">
				<div class="col-lg-5 col-md-5 text-center">
					<p>©
						<?php echo date('Y '); bloginfo('name'); ?> Todos los derechos reservados. Editorial Astrea</p>
				</div>
				<div class="col-lg-2 col-lg-offset-1 col-md-2 col-md-offset-1 text-center">
					<a href="mailto:contacto@lpp.mx">contacto@lpp.mx</a>
				</div>

				<div class="col-lg-3 col-lg-offset-1 col-md-13 col-md-offset-1 text-center">
					<a href="<?php bloginfo('url');  ?>">Acera de LA PRIMERA PLANA</a>
				</div>
			</div>

			<!--	FOOTER MOBILE	-->

			<div id="footerInfo" class="col-lg-12 col-md-12 hidden-md hidden-lg mobile text-left">
				<!--				<div class="col-lg-5 col-md-5">
				<div class="col-lg-2 col-lg-offset-1 col-md-2 col-md-offset-1">-->
				<a href="mailto:contacto@lpp.mx">contacto@lpp.mx</a>
				<!--				</div>-->
				<p>©
					<?php echo date('Y '); bloginfo('name'); ?>
					<br> Todos los derechos reservados. Editorial Astrea</p>
			</div>
		</div>
	</div>
	<!--	</div>-->
</footer>

<link rel="stylesheet" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.4.0/animate.min.css">
<!--	<link rel="stylesheet" media="screen" href="http://cdn.akoake.com/v/animations/2.1/css/animations.min.css">-->
	<script ype="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400" 
rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<!--	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-591a10780cdaf247"></script>-->

<?php wp_footer(); ?>
</body>
