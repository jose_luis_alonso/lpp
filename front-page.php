<?php get_header(); ?>


<?php get_template_part( 'elements/ads', 'skin-banner') ?>
<?php get_template_part( 'elements/ads', '720-home-top') ?>
<?php get_template_part( 'elements/ads', 'header-home-mobile') ?>

<div class="container" id="mainContent">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" id="leftContent">
			<?php get_template_part( 'elements/content', 'mainArticle'); ?>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" id="sideBarRight">
			<?php get_template_part('elements/content', 'firstArticle'); ?>
		</div>
		<?php get_template_part( 'elements/ads', 'square-home-sidebar') ?>
	</div>
</div>
<div class="container" id="categoryBoxes">
	<!--	<div class="row">-->
	<?php get_template_part( 'elements/content', 'categoryCard'); ?>
	<?php get_template_part( 'elements/content', 'categoryCard2ndRow'); ?>
	<!--	</div>-->
</div>

<?php get_template_part( 'elements/ads', '720-home-middle') ?>

<?php get_template_part( 'elements/content', 'videoSlider'); ?>
<div class="container" id="masReciente">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<h2 class="h2">Las noticias más recientes</h2>
		</div>
		<?php get_template_part('elements/content', 'newsRecentMain'); ?>
		<?php get_template_part( 'elements/ads', 'square-home-sidebar-bottom') ?>

		<div class="col-sm-12 col-md-8" id="masRecienteList">
			<div class="row">
				<?php get_template_part('elements/content', 'newsRecentListed'); ?>
			</div>
		</div>
		<div class="col-sm-12 col-md-4" id="masViral">
			<h2 class="h2">Lo más compartido</h2>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="masViralList">
				<ul>
					<?php get_template_part( 'elements/content', 'viralListed'); ?>
					<li class="fbPageBLock hidden-sm hidden-xs">
						<div class="fb-page" data-href="https://www.facebook.com/lppmx/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
							<blockquote cite="https://www.facebook.com/lppmx/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/lppmx/">La Primera Plana MX</a></blockquote>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<?php get_template_part( 'elements/ads', '720-home-bottom') ?>
<?php get_template_part( 'elements/content', 'contributors'); ?>
<?php get_template_part( 'elements/section', 'facebook') ?>
<?php get_template_part( 'elements/section', 'instagram') ?>
<?php get_template_part( 'elements/section', 'partnerSites') ?>
<?php get_template_part( 'elements/section', 'socialMedia') ?>
<?php get_footer(); ?>
