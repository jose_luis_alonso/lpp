<?php

//adjs(slick.minadd_category_to_single');
//adjs(slick.minadd_category_to_single');
//function add_category_to_single($classes, $class) {
//  if (is_single() ) {
//    global $post;
//    foreach((get_the_category($post->ID)) as $category) {
//      // add category slug to the $classes array
//      $classes[] = $category->cat_ID;
//    }
//  }
//  // return the $classes array
//  return $classes;
//}

function lpp_styles(){
	wp_enqueue_style( 'bootstrap-css', get_template_directory_uri(). '/css/bootstrap.min.css');
	wp_enqueue_style( 'flexslider-css', get_template_directory_uri(). '/flexslider/flexslider.css');
	wp_enqueue_style( 'slider-css', get_template_directory_uri(). '/css/slider.css');
	wp_enqueue_style( 'slick-css', get_template_directory_uri(). '/css/slick.css');
	wp_enqueue_style( 'slicktheme-css', get_template_directory_uri(). '/css/slick-theme.css');
	wp_enqueue_style( 'lpp2017-css', get_template_directory_uri(). '/css/lpp_style.css');
}

add_action( 'wp_enqueue_scripts', 'lpp_styles');

function lpp_scripts(){
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri().'/js/bootstrap.min.js', array( 'jquery'), '', true);
	wp_enqueue_script( 'easing-js', get_template_directory_uri().'/flexslider/demo/js/jquery.easing.js', array( 'jquery'), '', true);
	wp_enqueue_script( 'mousewheel-js', get_template_directory_uri().'/flexslider/demo/js/jquery.mousewheel.js', array( 'jquery'), '', true);
	wp_enqueue_script( 'flexslider-js', get_template_directory_uri().'/flexslider/jquery.flexslider.js', array( 'jquery'), '', true);
	wp_enqueue_script( 'slick-js', get_template_directory_uri().'/js/slick.min.js', array( 'jquery'), '', true);
	wp_enqueue_script( 'instafeed-js', get_template_directory_uri().'/js/instafeed.min.js', array( 'jquery'), '', true);
	wp_enqueue_script( 'flexmenu-js', get_template_directory_uri().'/js/flexmenu.min.js', array( 'jquery'), '', true);
	wp_enqueue_script( 'propietary-js', get_template_directory_uri().'/js/my_app.js', array( 'jquery'), '', true);
	
}

add_action ( 'wp_enqueue_scripts', 'lpp_scripts');

add_theme_support( 'menus');

function register_theme_menus(){
	register_nav_menus(
		array(
			'header-menu' => __( 'Header Menu'),
			'footer-menu' => __( 'Footer Menu')
		)
	);
}

add_action( 'init', 'register_theme_menus');

    function excerpt($limit) {
      $excerpt = explode(' ', get_the_excerpt(), $limit);
      if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'...';
      } else {
        $excerpt = implode(" ",$excerpt);
      }	
      $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
      return $excerpt;
    }
     
    function content($limit) {
      $content = explode(' ', get_the_content(), $limit);
      if (count($content)>=$limit) {
        array_pop($content);
        $content = implode(" ",$content).'...';
      } else {
        $content = implode(" ",$content);
      }	
      $content = preg_replace('/\[.+\]/','', $content);
      $content = apply_filters('the_content', $content); 
      $content = str_replace(']]>', ']]&gt;', $content);
      return $content;
    }

function my_cptui_featured_image_support() {
	// Replace these post type slugs with the actual slugs you need support for.
	$cptui_post_types = array( 'partner', 'post' );
	add_theme_support( 'post-thumbnails', $cptui_post_types );
}
add_action( 'after_setup_theme', 'my_cptui_featured_image_support' );

//ACTIVE MENU ITEMS
//add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
//
//function special_nav_class ($classes, $item) {
//    if (in_array('current-menu-item', $classes) ){
//        $classes[] = 'active ';
//    }
//    return $classes;
//}

//ACTIVE CHILD MENU ITEMS
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class ($classes, $item) {
    if (in_array('current-page-ancestor', $classes) || in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}

?>