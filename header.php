<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta property="og:url" content="<?php echo the_permalink(); ?>" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="<?php echo the_title(); ?>" />
	<?php
	global $post;
	function max_excerpt($text, $excerpt){
 		if ($excerpt) return $excerpt;
		$text = strip_shortcodes( $text );
		$text = apply_filters('the_content', $text);
 		$text = str_replace(']]>', ']]&gt;', $text);
 		$text = strip_tags($text);
 		$excerpt_length = apply_filters('excerpt_length', 40);
 		$excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
 		$words = preg_split("/[\s,]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
 		if ( count($words) > $excerpt_length ) {
 			array_pop($words);
 			$text = implode(' ', $words);
 			$text = $text . $excerpt_more;
 		} else {
 			$text = implode(' ', $words);
 		}
		return apply_filters('wp_trim_excerpt', $text, $raw_excerpt);
	}

	function max_image() {
 		$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '', '' );
 		if ( has_post_thumbnail($post->ID) ) {
 			$ogimage = $src[0];
 		} else {
 			global $post, $posts;
 			$ogimage = '';
 			//$output = preg_match_all('/<img.+src=['"]([^'"]+)['"].*>/i', $post->post_content, $matches);
 			//$ogimage = $matches[1][0];
 		}
 		if(empty($ogimage)) {
 			$ogimage = "http://lpp.mx/wp-content/uploads/2017/10/logocomlppfinal.png";
 		}
 		return $ogimage;
	}

	$excerpt = "";
	if(is_single()){
		$excerpt = max_excerpt($post->post_content, $post_excerpt);
	}
	?>
	<meta property="og:description" content="<?php echo $excerpt; ?>"/>
	<meta property="og:image" content="<?php echo max_image();?>" />
		
<!--	Google DoubleClick Scripts -->


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-16687191-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-16687191-2');
</script>

	
<!--	<?php the_ad(110272); ?>-->
	
	<!--  <meta property="og:image"         content="../assets/favicon/favicon-16x16.png" />-->
	<title>
		<?php wp_title( '|', true, 'right') ?>
		<?php bloginfo( 'name'); ?>
	</title>
	<?php wp_head(); ?>
	<link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_directory')?>/assets/favicon/favicon-16x16.png">
</head>

<body <?php body_class(); ?> >
	<div id="fb-root"></div>
	<script>
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s);
			js.id = id;
			js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.10";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));

	</script>
	<div class="container-fluid" id="navBar">
		<!--	<div class="container-fluid" id="navBar">-->
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
					<!--				<nav class="navbar navbar-default small" role="navigation">-->
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
						<a class="navbar-brand" href="<?php bloginfo('url')?>">
<!--						<img src="<?php bloginfo('template_directory')?>/assets/lpp-logo-one-line.png" alt="" class="img-responsive">-->
						</a>
						<div id="searchIcon" class="socialmedia-square hidden-sm hidden-md hidden-lg">
							<a href="#"><img src="<?php bloginfo('template_directory')?>/assets/header/Search_18px.png" alt="" class=""></a>
						</div>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse navbar-ex1-collapse">
						<?php
							$args = array (
								'menu' => 'header_menu',
								'menu_class' => 'nav navbar-nav',
								'container' => 'true'
							);
							 wp_nav_menu( $args );
							?>
					</div>
					<div class="socialmedia-group flex-container flex-row hidden-xs">
						<div class="socialmedia-square hidden-xs">
							<a href="<?php the_field('facebook_global_url', get_option('page_on_front')); ?>" target="_blank">
						<img src="<?php bloginfo('template_directory')?>/assets/header/Facebook_30px.png" alt="">
					</a>
						</div>
						<div class="socialmedia-square hidden-xs">
							<a href="<?php the_field('twitter_global_url', get_option('page_on_front')); ?>" target="_blank">
						<img src="<?php bloginfo('template_directory')?>/assets/header/Twitter_30px.png" alt="">
							</a>
						</div>
						<div class="socialmedia-square hidden-xs ">
							<a href="<?php the_field('instagram_global_url', get_option('page_on_front')); ?>" target="_blank">
						<img src="<?php bloginfo('template_directory')?>/assets/header/IG_30px.png" alt="">
				</a>
						</div>
						<div class="socialmedia-square hidden-xs" id="searchToggle">
							<a href="#"><img src="<?php bloginfo('template_directory')?>/assets/header/Search_18px.png" alt="" class=""></a>
						</div>
					</div>
					<!-- /.navbar-collapse -->
				</nav>
			</div>
		</div>
	</div>
	<div id="searchBox" class="">
		<?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
	</div>
	<div id="mySearchBox" class="hidden-lg hidden-md hidden-sm">
		<?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
	</div>
	<?php if(is_home()): ?>
	<div class="container">
		<div class="row">
			<div class="col-xs- col-sm- col-md- col-lg-12">
				<?php if(function_exists('ditty_news_ticker')){ditty_news_ticker(143138);} ?>
			</div>
		</div>
	</div>
	<?php endif; ?>
