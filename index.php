<?php get_header(); ?>
<div class="container banner-large">
	<div class="row">
		<div class="col-xs- col-sm- col-md-12 col-lg-12"> <img src="" alt="" class="img-responsive"> </div>
	</div>
</div>
<div class="container" id="mainContent">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" id="leftContent">
			<!--				<div id="mainArticleBox">-->

			<?php
						$args = array(
						'post_type' => 'post',
						'posts_per_page' => 1,
						'featured' => 'featured',
						);
						$lastPost = new WP_Query($args);
						if( $lastPost->have_posts() ):
						echo '<div id="mainArticleBox">';
						while( $lastPost->have_posts() ): $lastPost->the_post();
						get_template_part('content', 'featured-big');
						endwhile;
						echo '</div>';
						endif;
					?>


				<div class="hero-img" style="background-image: url(http://lorempixel.com/800/600/);">
					<!--           <img src="http://lorempixel.com/749/370/business" alt="" class="img-responsive">-->
					<div class="tag-id hidden-xs hidden-sm"><span>Noticias</span></div>
				</div>
				<div class="mainArticle-texts">
					<h1 class="h1">Two line main tag for Main Article</h1>
					<div class="excerpt"> <span>Excerpt main tag for Main Article</span> </div>
				</div>
				<!--				</div>-->
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" id="sideBarRight">
			<div id="firstArticle">
				<div class="hero-img">
					<img src="http://lorempixel.com/480/160" alt="" class="img-responsive">
				</div>
				<div class="tag-id"> <span>Entretenimiento</span></div>
				<div class="excerpt"><span>Excerpt main tag for First Article in Sidebar</span></div>
			</div>
			<div class="square-banner"> <img src="http://lorempixel.com/300/250/transport" alt="" class="img-responsive center-block"> </div>
		</div>
	</div>
</div>
<div class="container" id="categoryBoxes">
	<div class="row">
		<div class="col-sm-3 categoryCard">
			<div class="img" style="background-image: url(http://lorempixel.com/800/600/);">
				<!--        	<img src="http://lorempixel.com/480/120/nature" alt="" class="img-responsive">-->
			</div>
			<div class="texts">
				<div class="tag-id">
					<div class="catego-tag">Entretenimiento</div>
				</div>
				<div class="excerpt">
					<p>Excerpt main tag for Category Card 1</p>
				</div>
			</div>
		</div>
		<div class="col-sm-3 categoryCard">
			<div class="img" style="background-image: url(http://lorempixel.com/800/600/);">
				<!--        	<img src="http://lorempixel.com/480/120/nature" alt="" class="img-responsive">-->
			</div>
			<div class="texts">
				<div class="tag-id">
					<div class="catego-tag">Entretenimiento</div>
				</div>
				<div class="excerpt">
					<p>Excerpt main tag for Category Card 1</p>
				</div>
			</div>
		</div>
		<div class="col-sm-3 categoryCard">
			<div class="img" style="background-image: url(http://lorempixel.com/800/600/);">
				<!--        	<img src="http://lorempixel.com/480/120/nature" alt="" class="img-responsive">-->
			</div>
			<div class="texts">
				<div class="tag-id">
					<div class="catego-tag">Entretenimiento</div>
				</div>
				<div class="excerpt">
					<p>Excerpt main tag for Category Card 1</p>
				</div>
			</div>
		</div>
		<div class="col-sm-3 categoryCard">
			<div class="img" style="background-image: url(http://lorempixel.com/800/600/);">
				<!--        	<img src="http://lorempixel.com/480/120/nature" alt="" class="img-responsive">-->
			</div>
			<div class="texts">
				<div class="tag-id">
					<div class="catego-tag">Entretenimiento</div>
				</div>
				<div class="excerpt">
					<p>Excerpt main tag for Category Card 1</p>
				</div>
			</div>
		</div>
		<div class="col-sm-3 categoryCard flexBox">
			<div class="img"> <img src="http://lorempixel.com/480/120/nature" alt="" class="img-responsive">
			</div>
			<div class="texts flexBox flex-col">
				<div class="tag-id">
					<div class="catego-tag">Entretenimiento</div>
				</div>
				<div class="excerpt">
					<p>Excerpt main tag for Category Card</p>
				</div>
			</div>
		</div>
		<div class="col-sm-3 categoryCard flexBox">
			<div class="img"> <img src="http://lorempixel.com/480/120/nature" alt="" class="img-responsive">
			</div>
			<div class="texts flexBox flex-col">
				<div class="tag-id">
					<div class="catego-tag">Entretenimiento</div>
				</div>
				<div class="excerpt">
					<p>Excerpt main tag for Category Card</p>
				</div>
			</div>
		</div>
		<div class="col-sm-3 categoryCard flexBox">
			<div class="img"> <img src="http://lorempixel.com/480/120/nature" alt="" class="img-responsive">
			</div>
			<div class="texts flexBox flex-col">
				<div class="tag-id">
					<div class="catego-tag">Entretenimiento</div>
				</div>
				<div class="excerpt">
					<p>Excerpt main tag for Category Card</p>
				</div>
			</div>
		</div>
		<div class="col-sm-3 categoryCard flexBox">
			<div class="img"> <img src="http://lorempixel.com/480/120/nature" alt="" class="img-responsive">
			</div>
			<div class="texts flexBox flex-col">
				<div class="tag-id">
					<div class="catego-tag">Entretenimiento</div>
				</div>
				<div class="excerpt">
					<p>Excerpt main tag for Category Card</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container banner-large section">
	<div class="row">
		<div class="col-xs- col-sm- col-md-12 col-lg-12"> <img src="" alt="" class="img-responsive"> </div>
	</div>
</div>
<div class="container-fluid section  hidden-xs hidden-sm" id="videoCaroussel">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hidden-xs hidden-sm">
			<p class="text-center" id="videoTitle"><span class="txt-highlight">Los videos más interesantes,</span> en nuestro canal de YouTube</p>
			<section class="slider">
				<div class="flexslider">
					<ul class="slides text-center">
						<li>
							<!-- Video Card for slider -->
							<div class="videoCard">
								<div class="heroThumb">
									<div class="embed-responsive embed-responsive-16by9">
										<!--                                            <iframe width="585" height="366" src="https://www.youtube.com/embed/PN8il_zxNWM" frameborder="0" allowfullscreen></iframe>-->
									</div>
								</div>
								<div class="title text-center">
									<p>El espeluznante tráiler de “Eso” ya está aquí</p>
								</div>
								<div class="descript text-center">
									<p>La cinta dirigida por el argentino Andrés Muschietti llegará a los cines el 8 de septiembre.</p>
								</div>
							</div>
						</li>
						<li>
							<!-- Video Card for slider -->
							<div class="videoCard">
								<div class="heroThumb">
									<div class="embed-responsive embed-responsive-16by9">
										<!--                                            <iframe width="585" height="366" src="https://www.youtube.com/embed/PN8il_zxNWM" frameborder="0" allowfullscreen></iframe>-->
									</div>
								</div>
								<div class="title text-center">
									<p>El espeluznante tráiler de “Eso” ya está aquí</p>
								</div>
								<div class="descript text-center">
									<p>La cinta dirigida por el argentino Andrés Muschietti llegará a los cines el 8 de septiembre.</p>
								</div>
							</div>
						</li>
						<li>
							<!-- Video Card for slider -->
							<div class="videoCard">
								<div class="heroThumb">
									<div class="embed-responsive embed-responsive-16by9">
										<!--                                            <iframe width="585" height="366" src="https://www.youtube.com/embed/PN8il_zxNWM" frameborder="0" allowfullscreen></iframe>-->
									</div>
								</div>
								<div class="title text-center">
									<p>El espeluznante tráiler de “Eso” ya está aquí</p>
								</div>
								<div class="descript text-center">
									<p>La cinta dirigida por el argentino Andrés Muschietti llegará a los cines el 8 de septiembre.</p>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</section>
		</div>
	</div>
</div>
<div class="container" id="masReciente">
	<div class="row">
		<h2 class="h2">Las noticias más recientes</h2>
		
		hello
		
		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" id="recienteFeatured">
			<div class="row">
				<div id="recienteMainArticle">
					<div class="col-sm-6 title hidden-xs hidden-sm">Reciente - featured title</div>
					<div class="col-sm-6 img"> <img src="http://lorempixel.com/480/240" alt="" class="img-responsive">
						<div class="tag-id"> <span>Noticias</span> </div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4 square-banner"> <img src="http://lorempixel.com/350/250/transport" alt="" class="img-responsive center-block"> </div>
		<div class="col-sm-8" id="masRecienteList">
			<div class="row">
				<div class="col-sm-6 recienteCard">
					<div class="img"> <img src="http://lorempixel.com/480/240/nature" alt="" class="img-responsive">
						<div class="tag-id">
							<p>Entretenimiento</p>
						</div>
					</div>
					<div class="excerpt">
						<p>Excerpt main tag for Category Card</p>
					</div>
				</div>
				<div class="col-sm-6 recienteCard">
					<div class="img"> <img src="http://lorempixel.com/480/240/nature" alt="" class="img-responsive">
						<div class="tag-id">
							<p>Entretenimiento</p>
						</div>
					</div>
					<div class="excerpt">
						<p>Excerpt main tag for Category Card</p>
					</div>
				</div>
				<div class="col-sm-6 recienteCard">
					<div class="img"> <img src="http://lorempixel.com/480/240/nature" alt="" class="img-responsive">
						<div class="tag-id">
							<p>Entretenimiento</p>
						</div>
					</div>
					<div class="excerpt">
						<p>Excerpt main tag for Category Card</p>
					</div>
				</div>
				<div class="col-sm-6 recienteCard">
					<div class="img"> <img src="http://lorempixel.com/480/240/nature" alt="" class="img-responsive">
						<div class="tag-id">
							<p>Entretenimiento</p>
						</div>
					</div>
					<div class="excerpt">
						<p>Excerpt main tag for Category Card</p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4" id="masViral">
			<h2 class="h2">Lo más compartido</h2>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="masViralList">
				<ul>
					<li class="viral-note">
						<div class="row no-gutter">
							<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"> <img src="http://lorempixel.com/80/80/business" alt="" class="square-mini img-responsive"> </div>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> <span class="catego-tag">Entretenimiento</span> </div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> <span class="excerpt">12 Handy Tips For Generating Leads Through Cold Calling</span> </div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="container banner-large">
	<div class="row">
		<div class="col-xs- col-sm- col-md-12 col-lg-12"> <img src="" alt="" class="img-responsive"> </div>
	</div>
</div>
<div class="container" id="colaboradores">
	<div class="row">
		<h2 class="h2 text-center">Colaboradores</h2>
		<div class="col-xs- col-sm- col-md-3 col-lg-3">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> <img src="http://lorempixel.com/100/100/abstract" class="img-circle center-block" alt="Cinque Terre" width="100" height="100">
				<div class="colabName text-center">Name Surname</div>
				<div class="colabSubtitle text-center">Cdc Issues Health Alert Notice For Travelers To Usa From Hon</div>
			</div>
		</div>
		<div class="col-xs- col-sm- col-md-3 col-lg-3">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> <img src="http://lorempixel.com/100/100/abstract" class="img-circle center-block" alt="Cinque Terre" width="100" height="100">
				<div class="colabName text-center">Name Surname</div>
				<div class="colabSubtitle text-center">Cdc Issues Health Alert Notice For Travelers To Usa From Hon</div>
			</div>
		</div>
		<div class="col-xs- col-sm- col-md-3 col-lg-3">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> <img src="http://lorempixel.com/100/100/abstract" class="img-circle center-block" alt="Cinque Terre" width="100" height="100">
				<div class="colabName text-center">Name Surname</div>
				<div class="colabSubtitle text-center">Cdc Issues Health Alert Notice For Travelers To Usa From Hon</div>
			</div>
		</div>
		<div class="col-xs- col-sm- col-md-3 col-lg-3">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> <img src="http://lorempixel.com/100/100/abstract" class="img-circle center-block" alt="Cinque Terre" width="100" height="100">
				<div class="colabName text-center">Name Surname</div>
				<div class="colabSubtitle text-center">Cdc Issues Health Alert Notice For Travelers To Usa From Hon</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4" id="colabButtonBox"> <a id="colabButton" class="btn btn-default" href="#" role="button">Ver todos los colaboradores</a> </div>
	</div>
</div>
<div class="container hidden-xs hidden-sm" id="fbLike">
	<div class="row">
		<div class="col-sm-2 col-lg-2">
			<div class="flexBox">
				<div class="flexBox-item"> <img src="<?php bloginfo('template_directory')?>/assets/fb-thumb.png" alt="" class="img-responsive"> </div>
			</div>
		</div>
		<div class="col-sm-10 col-lg-10">
			<div class="flexBox">
				<div class="flexBox-item">
					<div id="fbTitle" class="text-center"> Las últimas noticias directamente en tu muro,
						<br> <span class="fbBold">a un like de distancia</span> </div>
					<div id="fbLikeBtn" class="text-center">
						<div class="blueBtn"></div> <span>A 3.1 millones de personas les gusta esto.</span> </div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid insta" id="instagram">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-lg-offset-1  hidden-xs hidden-sm" id="instaLogo"> <img src="<?php bloginfo('template_directory')?>/assets/ig-100-px.png" class="img-responsive center-block" alt=""> </div>
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
				<div class="instaTitle"> Disfruta de nuestro contenido exclusivo
					<br> en instagram: <span class="boldTxt hidden-xs hidden-sm"><a href="#">@primeraplanamx</a></span></div>

				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-lg-offset-1  hidden-md hidden-lg mobile" id="instaLogoMobile"> <img src="<?php bloginfo('template_directory')?>/assets/ig-100-px.png" class="img-responsive center-block" alt="">
				</div>
				<div class="boldTxt hidden-md hidden-lg text-center">
					<a href="#" class="">@primeraplanamx</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid insta" id="instaCaroussel">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div> </div>
		</div>
	</div>
</div>
<div class="container-fluid  hidden-xs hidden-sm" id="otrosSitios">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center"> <img src="<?php bloginfo('template_directory')?>/assets/logo-astrea-header.png" alt="" class="src img-responsive center-block">
			<p class="title">Visita nuestros otros sitios</p>
		</div>
		<div class="container" id="categoryBoxes">
			<div class="row">
				<div class="col-sm-3 categoryCard">
					<div class="img"> <img src="http://lorempixel.com/262/120/nature" alt="" class="img-responsive">
					</div>
					<div class="texts">
						<div class="tag-id"> <span>El Influencer</span> </div>
						<div class="excerpt">
							<p>Excerpt main tag for Category Card</p>
						</div>
					</div>
				</div>
				<div class="col-sm-3 categoryCard">
					<div class="img"> <img src="http://lorempixel.com/262/120/nature" alt="" class="img-responsive">
					</div>
					<div class="texts">
						<div class="tag-id"> <span>El Influencer</span> </div>
						<div class="excerpt">
							<p>Excerpt main tag for Category Card</p>
						</div>
					</div>
				</div>
				<div class="col-sm-3 categoryCard">
					<div class="img"> <img src="http://lorempixel.com/262/120/nature" alt="" class="img-responsive">
					</div>
					<div class="texts">
						<div class="tag-id"> <span>El Influencer</span> </div>
						<div class="excerpt">
							<p>Excerpt main tag for Category Card</p>
						</div>
					</div>
				</div>
				<div class="col-sm-3 categoryCard">
					<div class="img"> <img src="http://lorempixel.com/262/120/nature" alt="" class="img-responsive">
					</div>
					<div class="texts">
						<div class="tag-id"> <span>El Influencer</span> </div>
						<div class="excerpt">
							<p>Excerpt main tag for Category Card</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container section" id="socialMedia">
	<div class="row">
		<div class="col-xs- col-sm- col-md- col-lg-">
			<h2 class="h2 text-center">¿Olvidaste añadirnos?</h2>
			<p class="title text-center">Disfruta del contenido de La Primera Plana
				<br><span class="boldTxt">en tu red social favorita.</span></p>
			<div id="socialBtnBox" class="flex-parent flex-row flex-between hidden-xs hidden-sm">
				<a class="btn btn-default social-media-button  hidden-xs hidden-sm" id="facebook" href="#" role="button"><img src="<?php bloginfo('template_directory')?>/assets/facebook-icon.png" alt="">Facebook</a>
				<a class="btn btn-default social-media-button inverse  hidden-xs hidden-sm" id="instagram" href="#" role="button"><img src="<?php bloginfo('template_directory')?>/assets/ig-glyph-30-px.png" alt="">Instagram</a>
				<a class="btn btn-default social-media-button  hidden-xs hidden-sm" id="twitter" href="#" role="button"><img src="<?php bloginfo('template_directory')?>/assets/icon.png" alt="">Twitter</a>
			</div>

			<div class="col-lg-3 col-lg-offset-1 flex-parent flex-row flex-around hidden-md  hidden-lg" id="footerSocial">
				<div class="social-square">
					<a href="#"><img src="<?php bloginfo('template_directory')?>/assets/footer/Facebook_50px.png" alt=""></a>
				</div>
				<div class="social-square">
					<a href="#"><img src="<?php bloginfo('template_directory')?>/assets/footer/Twitter_50px.png" alt=""></a>
				</div>
				<div class="social-square">
					<a href="#"><img src="<?php bloginfo('template_directory')?>/assets/footer/IG_50px.png" alt=""></a>
				</div>
			</div>

		</div>
	</div>
</div>
<?php get_footer(); ?>
