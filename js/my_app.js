// Can also be used with $(document).ready()

jQuery('.navbar-nav').flexMenu({
	linkText: "Más",
	linkTitle: "Ver más",
	threshold: 3
});
jQuery(window).load(function () {
	jQuery('.flexslider').flexslider({
		animation: "slide",
		animationLoop: true,
		itemWidth: 585,
		itemMargin: 90,
		minItems: 2,
		maxItems: 3
	});
});
jQuery('#menu-alternate-menu').append(jQuery("#footerSocial").removeClass("hidden-xs hidden-sm").addClass("hidden-sm hidden-lg hidden-md"));

jQuery('#searchToggle').click(function () {
	jQuery('#searchBox').toggleClass('open');
	jQuery(this).toggleClass('light');
})

jQuery('#searchIcon').click(function () {
	jQuery('#mySearchBox').toggleClass('open');
	jQuery(this).toggleClass('light');
})

jQuery(document).ready(function () {
	jQuery('.flexslider .slides').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		//			autoplay: true,
		autoplaySpeed: 2000,
		//			dots: false,
		//			infinite: true,
		speed: 500,
		//			fade: true,
		//			cssEase: 'linear',
		centerMode: true,
		variableWidth: true,
		centerPadding: '35px',
		prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
		nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'
	});
});


var feed = new Instafeed({
	get: 'user',
	//        tagName: 'awesome',
	userId: 563891192,
	clientId: '8a97100f7c1d4a94a2d551205b3087dd',
	accessToken: '563891192.1677ed0.08d0f0ca855a4e46b6c11093b080719d',
	sortBy: 'random',
	//	resolution: 'thumbnail',
	//	resolution: 'low_resolution',
	template: '<div class="multiple-items"><a href="{{link}}" target="_blank"><img src="{{image}}" /></a></div>',
	//	template: '<div><a href="{{link}}"><img src="{{image}}" /></a></div>',
	limit: 8,
	after: function () {
		jQuery('#instafeed').slick({
			//			dots: false,
			infinite: true,
			speed: 300,
			slidesToShow: 7,
			slidesToScroll: 1,
			centerMode: true,
			//			variableWidth: true,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						//						arrows: false,
						slidesToShow: 5,
						slidesToScroll: 1,
					}
    }
				,
				{
					breakpoint: 764,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1
					}
    },
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1,
						centerPadding: '40px',
					}
    }
  ]
		});
	}
});

feed.run();