<?php get_header(); ?>

<div class="container banner-large">
	<div class="row">
		<div class="col-xs- col-sm- col-md-12 col-lg-12">
			<img src="" alt="" class="img-responsive">
		</div>
	</div>
</div>
	<div class="container" id="mainContent">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" id="leftContent">
		
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			 <?php the_title(); ?>
			 <?php the_content(); ?>
			 <?php echo get_the_date(); ?>

			<?php endwhile; ?>
			<?php endif; ?>
				
			</div>
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" id="sideBarRight">
				<?php get_template_part('elements/content', 'firstArticle'); ?>
			</div>
		</div>
	</div>
<div class="container" id="categoryBoxes">
	<div class="row">
		<?php get_template_part( 'elements/content', 'categoryCard'); ?>
		<?php get_template_part( 'elements/content', 'categoryCard2ndRow'); ?>		
	</div>
</div>
<div class="container banner-large section">
	<div class="row">
		<div class="col-xs- col-sm- col-md-12 col-lg-12"> <img src="" alt="" class="img-responsive"> </div>
	</div>
</div>
<div class="container-fluid section  hidden-xs hidden-sm" id="videoCaroussel">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hidden-xs hidden-sm">
			<p class="text-center" id="videoTitle"><span class="txt-highlight">Los videos más interesantes,</span> en nuestro canal de YouTube</p>
			<section class="slider">
				<div class="flexslider">
					<ul class="slides text-center">
						<li>
							<!-- Video Card for slider -->
							<div class="videoCard">
								<div class="heroThumb">
									<div class="embed-responsive embed-responsive-16by9">
										<!--                                            <iframe width="585" height="366" src="https://www.youtube.com/embed/PN8il_zxNWM" frameborder="0" allowfullscreen></iframe>-->
									</div>
								</div>
								<div class="title text-center">
									<p>El espeluznante tráiler de “Eso” ya está aquí</p>
								</div>
								<div class="descript text-center">
									<p>La cinta dirigida por el argentino Andrés Muschietti llegará a los cines el 8 de septiembre.</p>
								</div>
							</div>
						</li>
						<li>
							<!-- Video Card for slider -->
							<div class="videoCard">
								<div class="heroThumb">
									<div class="embed-responsive embed-responsive-16by9">
										<!--                                            <iframe width="585" height="366" src="https://www.youtube.com/embed/PN8il_zxNWM" frameborder="0" allowfullscreen></iframe>-->
									</div>
								</div>
								<div class="title text-center">
									<p>El espeluznante tráiler de “Eso” ya está aquí</p>
								</div>
								<div class="descript text-center">
									<p>La cinta dirigida por el argentino Andrés Muschietti llegará a los cines el 8 de septiembre.</p>
								</div>
							</div>
						</li>
						<li>
							<!-- Video Card for slider -->
							<div class="videoCard">
								<div class="heroThumb">
									<div class="embed-responsive embed-responsive-16by9">
										<!--                                            <iframe width="585" height="366" src="https://www.youtube.com/embed/PN8il_zxNWM" frameborder="0" allowfullscreen></iframe>-->
									</div>
								</div>
								<div class="title text-center">
									<p>El espeluznante tráiler de “Eso” ya está aquí</p>
								</div>
								<div class="descript text-center">
									<p>La cinta dirigida por el argentino Andrés Muschietti llegará a los cines el 8 de septiembre.</p>
								</div>
							</div>
						</li>
						<!--
							<li>
								<img src="images/kitchen_adventurer_lemon.jpg" />
							</li>
							<li>
								<img src="images/kitchen_adventurer_donut.jpg" />
							</li>
							<li>
								<img src="images/kitchen_adventurer_caramel.jpg" />
							</li>
-->
					</ul>
				</div>
			</section>
		</div>
	</div>
</div>
<div class="container" id="masReciente">
	<div class="row">
		<h2 class="h2">Las noticias más recientes</h2>
		
		<?php get_template_part('elements/content', 'newsRecentMain'); ?>
		
		<div class="col-sm-4 square-banner"> <img src="http://lorempixel.com/350/250/transport" alt="" class="img-responsive center-block"> </div>
		
		<div class="col-sm-8" id="masRecienteList">
			<div class="row">
				<?php get_template_part('elements/content', 'newsRecentListed'); ?>
			</div>
		</div>
		<div class="col-sm-4" id="masViral">
			<h2 class="h2">Lo más compartido</h2>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="masViralList">
				<ul>
					<?php get_template_part( 'elements/content', 'viralListed'); ?>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="container banner-large">
	<div class="row">
		<div class="col-xs- col-sm- col-md-12 col-lg-12"> <img src="" alt="" class="img-responsive"> </div>
	</div>
</div>
<div class="container" id="colaboradores">
	<div class="row">
		<h2 class="h2 text-center">Colaboradores</h2>
		
		<?php get_template_part( 'elements/content', 'contributors'); ?>
				
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4" id="colabButtonBox"> <a id="colabButton" class="btn btn-default" href="#" role="button">Ver todos los colaboradores</a> </div>
	</div>
</div>


<?php get_template_part( 'elements/section', 'facebook') ?>
<?php get_template_part( 'elements/section', 'instagram') ?>
<?php get_template_part( 'elements/section', 'partnerSites') ?>
<?php get_template_part( 'elements/section', 'socialMedia') ?>
<?php get_footer(); ?>
