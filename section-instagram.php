<?php 

	$IGUrl = get_field('instagram_global_url');

var_dump($IGUrl);

?>
<div class="container-fluid insta" id="instagram">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-lg-offset-1  hidden-xs hidden-sm" id="instaLogo">
				<a href="<?php get_field('instagram_global_url',get_option('page_for_posts')); ?>">
					<img src="<?php bloginfo('template_directory')?>/assets/ig-100-px.png" class="img-responsive center-block" alt="">
			</a>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
				<div class="instaTitle"> Disfruta de nuestro contenido exclusivo
					<br> en instagram: <span class="boldTxt hidden-xs hidden-sm"><a href="<?php the_field('instagram_global_url') ?>" target="_blank">@primeraplanamx</a></span></div>

				<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-lg-offset-1  hidden-md hidden-lg mobile" id="instaLogoMobile"> <img src="<?php bloginfo('template_directory')?>/assets/ig-100-px.png" class="img-responsive center-block" alt="">
				</div>
				<div class="boldTxt hidden-md hidden-lg text-center">
					<a href=" <?php the_field('instagram_global_url') ?>" class="" target="_blank">@primeraplanamx</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid insta" id="instaCaroussel">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div>
				<?php echo do_shortcode( '[instagram-feed num=7 cols=7 id="563891192" class=grammySlider sortby=random num=7]'); ?>
			</div>
		</div>
	</div>
</div>
