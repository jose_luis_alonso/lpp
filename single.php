<?php

/**
*Theme Name: LPP2017
*Author: Mau Ferrusca / Dorian Martíneza
*Author URI: http://wordpress.org/
*Description:  Skin responsivo y theme para LPP 2017.
*Version: 1.0
*License: GNU General Public License v2 or later
*License URI: http://www.gnu.org/licenses/gpl-2.0.html
*Tags: white, responsive, bootstrap, ACF
*Template Name: Single post
*/

get_header(); ?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php
			function catName(){
				$category = get_the_category();
				echo '<span>' . $category[0]->name . '</span>';
			}
	?>

	<div class="container-fluid hidden-xs" id="mainNews" style="background-image: url(<?php  echo get_the_post_thumbnail_url(); ?>">
		<div class="row">
			<div class="col-sm-5 col-sm-offset-1" id="texts">
				<div class="cat-date inner">
					<span class="cat">
						<?php catName(); ?>
					</span>
					<span>/</span>
					<span class="date"><?php echo get_the_date(); ?></span>
				</div>
				<div class="headline h1  inner">
					<?php echo get_the_title(); ?>			
				</div>
				<div class="extract inner">
					<?php echo excerpt(25) ?> 
				</div>
			</div>
			<div class="col-sm-5" id="featuredImg">
				<div id="singleFeaturedImg" class="img img-responsive" style="background-image: url(<?php  echo get_the_post_thumbnail_url(); ?>">

					<div class="tag-id"> <span><?php catName(); ?></span></div>
				</div>
			</div>

		</div>
	</div>
	
		<div class="container  hidden-lg hidden-md hidden-sm" id="mainContent">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" id="leftContent">
				<div id="mainArticleBox">
					<div class="hero-img" style="background-image: url(<?php  echo get_the_post_thumbnail_url(); ?>">
						<!--           <img src="http://lorempixel.com/749/370/business" alt="" class="img-responsive">-->
						<div class="tag-id hidden-xs hidden-sm"><span><?php catName(); ?></span></div>
					</div>
					<div class="mainArticle-texts">
						<h1 class="h1"><?php echo get_the_title(); ?></h1>
						<div class="excerpt"> <span><?php echo excerpt(25) ?> </span> </div>
					</div>
				</div>
			</div>

		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" id="leftContent">
				<div id="mainNewsContent">
<!--
					<div class="cat-date inner">
											<span class="cat">
						<?php
						
						$category = get_the_category();
					echo '<a href="'.get_category_link($category[0]->cat_ID).'">' . $category[0]->cat_name . '</a>';
						?>
					</span>
						<span>/</span>
						<span class="date"><?php echo get_the_date(); ?></span>
					</div>
-->
 					
 					<?php echo the_content(); ?>

				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" id="sideBarRight">
			<?php get_template_part( 'elements/ads', 'square-home-sidebar-bottom') ?>
			</div>
		</div>
	</div>			
			<?php endwhile; ?>
			<?php endif; ?>
<?php get_template_part( 'elements/section', 'waitContent') ?>
<?php get_template_part( 'elements/section', 'facebook') ?>
<?php get_template_part( 'elements/ads', '720-home-bottom') ?>
<?php get_template_part( 'elements/section', 'instagram') ?>
<?php get_template_part( 'elements/section', 'socialMedia') ?>
<?php get_footer(); ?>
